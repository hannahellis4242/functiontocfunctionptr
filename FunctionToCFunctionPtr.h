#ifndef FUNCTIONTOCFUNCTIONPTR
#define FUNCTIONTOCFUNCTIONPTR

/*
The MIT License (MIT)

Copyright (c) 2016 Adam Michael Ellis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <functional>
#include <memory>

template <class FuncType>
class FunctionToCFunctionPtrSingleton;

template <class R,class ...Args>
class FunctionToCFunctionPtrSingleton<R(Args...)>
{
    private:
        static std::function<R(Args...)> func;
		static std::shared_ptr<FunctionToCFunctionPtrSingleton<R(Args...)>> instance;

        //disallow construction,copy and asignment
		FunctionToCFunctionPtrSingleton(){}

		FunctionToCFunctionPtrSingleton(const FunctionToCFunctionPtrSingleton &);
		FunctionToCFunctionPtrSingleton &operator =(const FunctionToCFunctionPtrSingleton &);
    public:
		~FunctionToCFunctionPtrSingleton()
        {
        }
		static const FunctionToCFunctionPtrSingleton& getInstance(const std::function<R(Args...)> &f)
        {
            if(!instance)
            {
				instance=std::shared_ptr<FunctionToCFunctionPtrSingleton<R(Args...)>>(new FunctionToCFunctionPtrSingleton<R(Args...)>);
            }
            func=f;
			return *instance;
        }

		static R call(Args ...args)
        {
			return func(args...);
        }
};

template <class R,class ...Args>
std::shared_ptr<FunctionToCFunctionPtrSingleton<R(Args...)>> FunctionToCFunctionPtrSingleton<R(Args...)>::instance;

template <class R,class ...Args>
std::function<R(Args...)> FunctionToCFunctionPtrSingleton<R(Args...)>::func;

template<class T>
struct cFuncPtr;

template<class R,class ...Args>
struct cFuncPtr<R(Args...)>
{
		typedef R(*ptr)(Args...);
};

template <class FuncType>
typename cFuncPtr<FuncType>::ptr FunctionToCFunctionPtr(const std::function<FuncType> &f)
{
	return &(FunctionToCFunctionPtrSingleton<FuncType>::getInstance(f).call);
}

#endif // FUNCTIONTOCFUNCTIONPTR

